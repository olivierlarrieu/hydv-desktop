import os
from libs.decorators import func_log
from libs.gui.window import HybWin
from libs.utils import monitors, loop_start, loop_stop

MAIN_MONITOR = monitors[0]
DIR_PATH = os.path.dirname(os.path.realpath(__file__))


class ToolsDash(HybWin):
    template = os.path.join(DIR_PATH, "templates", "index.html")
    width = 300
    height = MAIN_MONITOR.height - 50
    pos_x = MAIN_MONITOR.x + MAIN_MONITOR.width - 300
    pos_y = 50
    type_hint = 8
    is_above = True
    title = 'title'
