import os
from libs.decorators import func_log
from libs.gui.window import HybWinAutoShow, HybWin
from libs.utils import monitors, screen_height, screen_width, loop_start, loop_stop

MAIN_MONITOR = monitors[0]
DIR_PATH = os.path.dirname(os.path.realpath(__file__))


class Panel(HybWinAutoShow):
    template = os.path.join(DIR_PATH, "templates", "index.html")
    width = MAIN_MONITOR.width
    height = 50
    pos_x = MAIN_MONITOR.x
    pos_y = 0
    type_hint = 8
    is_above = True
    title = 'title'
    whith_scroll_bar = False
