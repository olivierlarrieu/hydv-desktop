
from libs.decorators import func_log
from libs.gui.window import Action
from libs.utils import loop_stop


class WindowsManager():
    
    def __init__(self, apps_dash, panel, tools_dash):
        self.apps_dash = apps_dash
        self.panel = panel
        self.tools_dash = tools_dash

        self.panel.register_action(Action("open_apps_dash", self.open_apps_dash_action))
        self.panel.register_action(Action("open_tools_dash", self.open_tools_dash_action))
        self.panel.register_action(Action("panel_to_top", self.open_apps_dash_action))
        self.panel.register_action(Action("panel_to_bottom", self.open_tools_dash_action))
        self.panel.register_action(Action("kill", self.kill_action))

    @func_log
    def open_apps_dash_action(self, name, data):
        if self.apps_dash.is_visible:
            self.apps_dash.hide()
            # TODO: manage xlib space reservation
        else:
            self.apps_dash.show_all()
            # TODO: manage xlib space reservation

    @func_log
    def open_tools_dash_action(self, name, data):
        if self.tools_dash.is_visible:
            self.tools_dash.hide()
            # TODO: manage xlib space reservation
        else:
            self.tools_dash.show_all()
            # TODO: manage xlib space reservation

    @func_log
    def kill_action(self, name, data):
        loop_stop()

    @func_log
    def panel_to_top_action(self, name, data):
        """
        move the panel to top and adjust apps_dash and tools_dash position
        manage xlib space reservation
        """
        pass

    @func_log
    def panel_to_bottom_action(self, name, data):
        """
        move the panel to bottom and adjust apps_dash and tools_dash position
        manage xlib space reservation
        """
        pass


class CommunicationManager():
    """
    manage communication beetween windows
    eg: add shortcut app from panel to apps_dash, notify etc...
    """

    def __init__(self, apps_dash, panel, tools_dash):
        self.apps_dash = apps_dash
        self.panel = panel
        self.tools_dash = tools_dash
