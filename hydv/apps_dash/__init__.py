import os
from libs.decorators import func_log
from libs.gui.window import HybWin
from libs.utils import monitors, screen_height, screen_width, loop_start, loop_stop

MAIN_MONITOR = monitors[0]
DIR_PATH = os.path.dirname(os.path.realpath(__file__))


class AppsDashTooltip(HybWin):
    template = os.path.join(DIR_PATH, "templates", "index.html")
    width = 200
    height = 200
    pos_x = MAIN_MONITOR.x + 200
    pos_y = 200
    type_hint = 8
    is_above = True
    title = 'title'
    is_visible = False


class AppsDash(HybWin):
    template = os.path.join(DIR_PATH, "templates", "index.html")
    width = 50
    height = screen_height - 50
    pos_x = MAIN_MONITOR.x
    pos_y = 50
    type_hint = 8
    is_above = True
    title = 'title'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tooltip = AppsDashTooltip()
