# hydv desktop

    hydv is a desktop environment writtem with python3 and Gtk, WebKit.
    It should be more soft than modern desktop which work with composition and a laggy on
    small systems.

    The development and tuning of interfaces should be as simple than develop front end apps.

    this repository is only for hydv desktop purposes.
    gui libs and tools for hydv will take place here in a first version.

    In a second version the hydv desktop will have it s own repository.
    libs/ will have it s own repository and will be used as a reusable librairy.

    An Hybryde desktop switcher will be in an another and agnostic repository, it ll will be a cli
    and a python librairy used by hydv.

# The Panel

    A classic panel which will display:
        - Applications menu
        - buttons to open Apps Dash and Tools Dash
        - Hybryde switch desktop window
        - networks, battery, notifications infos

# The Apps Dash

    A dash to access Favorites appication like unity left panel

# The Tools Dash

    A Tools access window, desktop preferences, file folder ....
