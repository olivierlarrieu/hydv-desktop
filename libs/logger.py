import logging
import sys
from logging.handlers import RotatingFileHandler


logger = logging.getLogger('Hybryde')
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')

if "debug" in sys.argv:
    file_handler = RotatingFileHandler('hydv.log', 'a', 1000000, 1)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
 
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.DEBUG)
logger.addHandler(stream_handler)
