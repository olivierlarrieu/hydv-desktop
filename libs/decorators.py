from .logger import logger


def func_log(func):

    def wrapper(*args, **kwargs):
        
        elems = []

        if args:
            elems.append(args)
        if kwargs:
            elems.append(kwargs)

        logger.info('{} :: {}'.format(func.__name__, elems))

        try:
            func(*args, **kwargs)
        except Exception as e:
            logger.error(e)

    return wrapper
