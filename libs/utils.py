from gi.repository import Gtk, Gdk

win = Gdk.get_default_root_window()
screen = win.get_screen()
geometry = screen.get_monitor_geometry(0)

screen_height = geometry.height
screen_width = geometry.width

n_monitor = screen.get_n_monitors()
monitors = [screen.get_monitor_geometry(i) for i in range(n_monitor)]


def loop_start():
    Gtk.main()

def loop_stop():
    Gtk.main_quit()