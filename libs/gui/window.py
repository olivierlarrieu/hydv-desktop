#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Contact: olivier.hybryde.dev@gmail.com
"""
import gi
import json
import os
from gi.repository import Gtk 
from gi.repository import Gdk 
from gi.repository import WebKit
from gi.repository import GLib
from libs.decorators import func_log

gi.require_version('Gtk', '3.0')
gi.require_version('WebKit', '3.0')

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
emmiterjs = open(os.path.join(DIR_PATH, "emmiter.js")).read()


class HybBrowser(WebKit.WebView):
    def __init__(self, width, height, template, actions):
        super().__init__()

        self.actions = actions
        self.template = template
        self.set_transparent(True)
        self.set_app_paintable(True)
        self.set_size_request(width, height)
        browser_settings = self.get_settings()
        settings_white_list = ('enable-frame-flattening',
                              'enable-page-cache',
                              'tab-key-cycles-through-elements',
                              'enable-universal-access-from-file-uris',
                              'enable-offline-web-application-cache',
                              'enable-caret-browsing',
                              # 'enable-plugins',
                              'enable-scripts',
                              'auto-shrink-images',
                              'auto-load-images',
                              'enforce-96-dpi',
                              # 'enable-private-browsing',
                              'enable-spell-checking',
                              'enable-caret-browsing',
                              # 'enable-html5-database',
                              # 'enable-html5-local-storage',
                              'enable-xss-auditor',
                              'enable-spatial-navigation',
                              'enable-frame-flattening',
                              # 'javascript-can-access-clipboard',
                              # 'enable-offline-web-application-cache',
                              'enable-universal-access-from-file-uris',
                              'enable-file-access-from-file-uris',
                              # 'enable-site-specific-quirks',
                              # 'enable-page-cache',
                              'auto-resize-window',
                              # 'enable-java-applet',
                              # 'enable-hyperlink-auditing',
                              'enable-fullscreen',
                              # 'enable-accelerated-compositing',
                              #'enable_webgl', 'editing-behavior','tab-key-cycles-through-elements',
                              )

        settings_black_list = ('enable-default-context-menu',
                               'enable-developer-extras',
                               'javascript-can-open-windows-automatically',
                               
                               'enable-dom-paste',
                               
                               'enable-default-context-menu',
                               'enable-dns-prefetching',)
        
        for setting in settings_white_list:
            browser_settings.set_property(setting, True)
        browser_settings.set_property("editing-behavior", 3)
        for setting in settings_black_list:
            browser_settings.set_property(setting, False)
           
        self.set_settings(browser_settings)

        with open(template, 'r') as string:
            template = string.read()
            self.load_string(template, "text/html", "utf-8", "file://" + self.template)

        self.connect("title-changed", self.on_title_changed)
        self.connect("navigation-policy-decision-requested", self.request_browser)
        self.execute_script(emmiterjs)


    @func_log
    def on_title_changed(self, widget, frame, action_name):
        print("action_name:", action_name)
        action_name, data = action_name.split('::')
        print("action_name:", action_name)
        if data:
            data = json.loads(data)
        if action_name == "refresh":
            return
        for action in self.actions:
            action.execute(action_name, data)

    @func_log
    def request_browser(self, widget, widget1, request, widget2, widget3, data=None):
        # hack to disable drop in webview
        uri = request.get_uri()
        if uri != "file://" + self.template:
            self.stop_loading()


class Action():
    """
    Action is an object which made possible the communication beetween the javascript webview
    and the python code.
    """
    def __init__(self, name, func):
        self.name = name
        self.func = func

    @func_log
    def execute(self, action_name, data):
        if action_name == self.name:
            self.func(action_name, data)


class HybWin(Gtk.Window):

    template = ""
    width = 0
    height = 0
    pos_x = 0
    pos_y = 0
    type_hint = 6
    is_above = True
    title = ''
    whith_scroll_bar = True
    is_visible = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.actions = []
        self.init_window()
        self.init_webview()
        self.init_transparency()
        self.realize()
        self.move(self.pos_x, self.pos_y)
        self.grab_focus()

    @func_log
    def init_window(self):
        self.set_title(self.title)
        self.set_decorated(False)
        self.set_keep_above(self.is_above)
        self.set_type_hint(Gdk.WindowTypeHint(self.type_hint))
        self.set_size_request(self.width, self.height)
        self.set_skip_taskbar_hint(True)
        self.set_skip_pager_hint(True)
        self.set_resizable(False)
        self.stick()

    @func_log
    def init_webview(self):
        self.webview = HybBrowser(self.width, self.height, self.template, self.actions)
        self.webview.set_size_request(self.width, self.height)

        if self.whith_scroll_bar:
            self.scroll = Gtk.ScrolledWindow()
            self.scroll.set_size_request(self.width, self.height)
            self.bar= self.scroll.get_vscrollbar()
            self.bar.set_child_visible(False) 
            self.scroll.add(self.webview)
            self.box = Gtk.VBox()
            self.box.add(self.scroll)
            self.add(self.box)    
        else:
            self.add(self.webview)

    @func_log
    def init_transparency(self):     
        self.screen = self.get_screen()
        self.visual = self.screen.get_rgba_visual()
        if self.visual != None and self.screen.is_composited():
            self.set_visual(self.visual)
            self.set_app_paintable(True)

    @func_log
    def register_action(self, action):
        if not isinstance(action, Action):
            raise Exception("action should be Action instance.")
        self.actions.append(action)
        self.webview.actions = self.actions

    @func_log
    def show_all(self):
        self.is_visible = True
        super().show_all()

    @func_log
    def hide(self):
        self.is_visible = False
        super().hide()


class HybWinAutoShow(HybWin):
    is_visible = True

    def __init__(self, *args, **kwargs):
        
        super().__init__(*args, **kwargs)
        self.show_all()
