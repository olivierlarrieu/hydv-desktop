from hydv.apps_dash import AppsDash
from hydv.managers import WindowsManager, CommunicationManager
from hydv.panel import Panel
from hydv.tools_dash import ToolsDash
from libs.utils import loop_start


class HyDesktop():

    def __init__(self, *args, **kwargs):

        self.apps_dash = AppsDash()
        self.panel = Panel()
        self.tools_dash = ToolsDash()

        # this manager only manage windows position and displays
        self.windows_manager = WindowsManager(self.apps_dash, self.panel, self.tools_dash)

        # this manager only manage communication beetween windows
        self.communication_manager = CommunicationManager(self.apps_dash, self.panel, self.tools_dash)


if __name__ == "__main__":

    hydv1 = HyDesktop()
    loop_start()
